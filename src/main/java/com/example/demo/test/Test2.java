package com.example.demo.test;

import org.jose4j.json.internal.json_simple.JSONObject;
import org.jose4j.json.internal.json_simple.parser.JSONParser;
import org.jose4j.json.internal.json_simple.parser.ParseException;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Acer on 6/30/2018.
 */
@RestController
public class Test2 {
    @GetMapping(value = "api/edit-profile/")
    public JSONObject editpro(HttpServletRequest request){
        System.out.println(request.getHeader("Authorization"));
        String s= "{  \n" +
                "   \"success\":false,\n" +
                "   \"user\":24,\n" +
                "   \"token\":\"8b6da13dc8d9e6ae7e8cd577f0a5ef02ac0ecf2c\",\n" +
                "   \"userData\":{  \n" +
                "      \"id\":24,\n" +
                "      \"first_name\":\"Razin\",\n" +
                "      \"last_name\":\"Sakib\",\n" +
                "      \"full_name\":\"Razin Bashar Sakib\",\n" +
                "      \"phone\":\"01724131216\",\n" +
                "      \"email\":\"razinbuet334@gmail.com\",\n" +
                "      \"avatar_url\":\"https:\\/\\/s3-ap-southeast-1.amazonaws.com\\/bloodfriend-profile-picture\\/9b2aaad4-7b7d-11e8-8394-ea00c0982401.jpeg\",\n" +
                "      \"userdonation\":{  \n" +
                "         \"blood_group\":\"O+\",\n" +
                "         \"donation_status\":\"reserved\",\n" +
                "         \"donation_times\":0,\n" +
                "         \"donation_last\":null\n" +
                "      },\n" +
                "      \"locations_data\":[  \n" +
                "         {  \n" +
                "            \"country\":\"Bangladesh\",\n" +
                "            \"state\":null,\n" +
                "            \"province\":null,\n" +
                "            \"district\":null,\n" +
                "            \"city\":null,\n" +
                "            \"address\":null,\n" +
                "            \"latitude\":23.7451099,\n" +
                "            \"longitude\":90.3731415\n" +
                "         }\n" +
                "      ],\n" +
                "      \"userinfo\":{  \n" +
                "         \"school\":\"kope\",\n" +
                "         \"college\":\"poiuy\",\n" +
                "         \"university\":\"asdf\",\n" +
                "         \"job\":\"abc\"\n" +
                "      },\n" +
                "      \"cover_url\":\"https:\\/\\/s3-ap-southeast-1.amazonaws.com\\/bloodfriend-profile-picture\\/4797f830-7b7e-11e8-aeea-ea00c0982401.jpeg\"\n" +
                "   }\n" +
                "}";

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) jsonParser.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @PostMapping(value = "api/search-global/")
    public JSONObject search(@RequestParam String query){
        System.out.println(query);
        String s= "{\n" +
                "    \"success\": true,\n" +
                "    \"user\": 24,\n" +
                "    \"token\": \"8b6da13dc8d9e6ae7e8cd577f0a5ef02ac0ecf2c\",\n" +
                "    \"usersMatched\": [\n" +
                "        {\n" +
                "            \"id\": 15,\n" +
                "            \"first_name\": \"Mehran\",\n" +
                "            \"last_name\": \"Kader\",\n" +
                "            \"slug\": \"mehran-kader-1\",\n" +
                "            \"full_name\": \"Mehran Kader\",\n" +
                "            \"phone\": \"01730019842\",\n" +
                "            \"email\": \"mehran.kingfrost@gmail.com\",\n" +
                "            \"avatar_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/default_bf.png\",\n" +
                "            \"userdonation\": {\n" +
                "                \"blood_group\": \"A+\",\n" +
                "                \"donation_status\": \"ready\",\n" +
                "                \"donation_times\": 0,\n" +
                "                \"donation_last\": null\n" +
                "            },\n" +
                "            \"locations_data\": [\n" +
                "                {\n" +
                "                    \"country\": \"Bangladesh\",\n" +
                "                    \"state\": null,\n" +
                "                    \"province\": null,\n" +
                "                    \"district\": null,\n" +
                "                    \"city\": null,\n" +
                "                    \"address\": null,\n" +
                "                    \"latitude\": 23.7451259,\n" +
                "                    \"longitude\": 90.3730927\n" +
                "                }\n" +
                "            ],\n" +
                "            \"userinfo\": {\n" +
                "                \"school\": null,\n" +
                "                \"college\": null,\n" +
                "                \"university\": null,\n" +
                "                \"job\": \"sad\"\n" +
                "            },\n" +
                "            \"cover_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/default_cp.png\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 18,\n" +
                "            \"first_name\": \"Mehran\",\n" +
                "            \"last_name\": \"Kader\",\n" +
                "            \"slug\": \"mehran-kader-2\",\n" +
                "            \"full_name\": \"Mehran Kader\",\n" +
                "            \"phone\": \"01730479817\",\n" +
                "            \"email\": \"kmehran.1106@gmail.com\",\n" +
                "            \"avatar_url\": null,\n" +
                "            \"userdonation\": {\n" +
                "                \"blood_group\": \"A-\",\n" +
                "                \"donation_status\": \"recuperating\",\n" +
                "                \"donation_times\": 0,\n" +
                "                \"donation_last\": null\n" +
                "            },\n" +
                "            \"locations_data\": [\n" +
                "                {\n" +
                "                    \"country\": \"Bangladesh\",\n" +
                "                    \"state\": null,\n" +
                "                    \"province\": null,\n" +
                "                    \"district\": null,\n" +
                "                    \"city\": null,\n" +
                "                    \"address\": null,\n" +
                "                    \"latitude\": 23.7451099,\n" +
                "                    \"longitude\": 90.3731415\n" +
                "                }\n" +
                "            ],\n" +
                "            \"userinfo\": {\n" +
                "                \"school\": \"damn\",\n" +
                "                \"college\": \"low\",\n" +
                "                \"university\": \"sad\",\n" +
                "                \"job\": \"habi jabi\"\n" +
                "            },\n" +
                "            \"cover_url\": null\n" +
                "        }\n" +
                "    ],\n" +
                "    \"circlesMatched\": []\n" +
                "}";

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) jsonParser.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }


}
