package com.example.demo.test;

import org.jose4j.json.internal.json_simple.JSONObject;
import org.jose4j.json.internal.json_simple.parser.JSONParser;
import org.jose4j.json.internal.json_simple.parser.ParseException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Asus on 6/30/2018.
 */
@RestController
public class Circle {
    @GetMapping(value = "api/circle/membership/")
    public JSONObject ciclelist(HttpServletRequest request){
        System.out.println(request.getHeader("Authorization"));
        String s= "{\n" +
                "    \"success\": true,\n" +
                "    \"user\": 15,\n" +
                "    \"token\": \"8b6da13dc8d9e6ae7e8cd577f0a5ef02ac0ecf2f\",\n" +
                "    \"circleData\": [\n" +
                "        {\n" +
                "            \"id\": 1,\n" +
                "            \"name\": \"test\",\n" +
                "            \"creator\": 16,\n" +
                "            \"description\": \"sadlife\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 2,\n" +
                "            \"name\": \"firstOne\",\n" +
                "            \"creator\": 15,\n" +
                "            \"description\": \"sadlife\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 3,\n" +
                "            \"name\": \"secondOne\",\n" +
                "            \"creator\": 15,\n" +
                "            \"description\": \"sadlife2\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) jsonParser.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @GetMapping(value = "api/circle/index/")
    public ModelAndView cicleindex(HttpServletRequest request){

        ModelAndView modelAndView = new ModelAndView("upload");
        return modelAndView;
    }

    @GetMapping(value = "api/circle/{id}")
    public JSONObject circle(@PathVariable String id){
        System.out.println(id);
        String s= "{\n" +
                "    \"success\": true,\n" +
                "    \"user\": 15,\n" +
                "    \"token\": \"8b6da13dc8d9e6ae7e8cd577f0a5ef02ac0ecf2f\",\n" +
                "    \"is_admin\": true,\n" +
                "    \"circleData\": {\n" +
                "        \"id\": 2,\n" +
                "        \"name\": \"firstOne\",\n" +
                "        \"creator\": 15,\n" +
                "        \"description\": \"sadlife\"\n" +
                "    },\n" +
                "    \"pendingMembers\": [],\n" +
                "    \"circleMessage\": \"Member\",\n" +
                "    \"circleStatus\": true,\n" +
                "    \"circleValue\": 1,\n" +
                "   \"postData\":[  \n" +
                "      {  \n" +
                "         \"at_user\":{  \n" +
                "            \"full_name\":\"Razin Bashar Sakib\",\n" +
                "            \"avatar_url\":\"https://api.androidhive.info/json/movies/1.jpg\",\n" +
                "\t\t\t\"slug\": \"razin-bashar-sakib-1\",\n" +
                "            \"phone\":\"01724131216\",\n" +
                "            \"userdonation\":{  \n" +
                "               \"donation_times\":0,\n" +
                "               \"donation_last\":null,\n" +
                "               \"donation_status\":\"ready\",\n" +
                "               \"blood_group\":\"O+\"\n" +
                "            },\n" +
                "            \"last_name\":\"Sakib\",\n" +
                "            \"id\":24,\n" +
                "            \"first_name\":\"Razin\",\n" +
                "            \"email\":\"razinbuet334@gmail.com\"\n" +
                "         },\n" +
                "         \"created_at\":\"2018-05-18T15:20:00.590276\",\n" +
                "         \"post_comments\":[  \n" +
                "            {  \n" +
                "               \"created_at\":\"2018-05-18T15:20:00.606797\",\n" +
                "               \"writer\":{  \n" +
                "                  \"full_name\":\"Rook Chessman\",\n" +
                "                  \"avatar_url\":\"https://api.androidhive.info/json/movies/1.jpg\",\n" +
                "\t\t\t\t  \"slug\": \"rook-chessman-1\",\n" +
                "                  \"phone\":\"01730479817\",\n" +
                "                  \"userdonation\":{  \n" +
                "                     \"donation_times\":0,\n" +
                "                     \"donation_last\":null,\n" +
                "                     \"donation_status\":null,\n" +
                "                     \"blood_group\":\"A+\"\n" +
                "                  },\n" +
                "                  \"last_name\":\"Chessman\",\n" +
                "                  \"id\":17,\n" +
                "                  \"first_name\":\"Rook\",\n" +
                "                  \"email\":\"rookchessman@gmail.com\"\n" +
                "               },\n" +
                "               \"message\":\"zzz\"\n" +
                "            },\n" +
                "            {  \n" +
                "               \"created_at\":\"2018-05-18T15:20:00.606797\",\n" +
                "               \"writer\":{  \n" +
                "                  \"full_name\":\"Rooky\",\n" +
                "                  \"avatar_url\":\"https://api.androidhive.info/json/movies/1.jpg\",\n" +
                "\t\t\t\t  \"slug\": \"razin-bashar-sakib-1\",\n" +
                "                  \"phone\":\"01730479817\",\n" +
                "                  \"userdonation\":{  \n" +
                "                     \"donation_times\":0,\n" +
                "                     \"donation_last\":null,\n" +
                "                     \"donation_status\":null,\n" +
                "                     \"blood_group\":\"A+\"\n" +
                "                  },\n" +
                "                  \"last_name\":\"Chessman\",\n" +
                "                  \"id\":17,\n" +
                "                  \"first_name\":\"Rooky\",\n" +
                "                  \"email\":\"rookchessman@gmail.com\"\n" +
                "               },\n" +
                "               \"message\":\"yahoo i get it\"\n" +
                "            },\n" +
                "            {  \n" +
                "               \"created_at\":\"2018-05-18T15:20:00.606797\",\n" +
                "               \"writer\":{  \n" +
                "                  \"full_name\":\"udaya\",\n" +
                "                  \"avatar_url\":\"https://api.androidhive.info/json/movies/1.jpg\",\n" +
                "\t\t\t\t  \"slug\": \"udaya-1\",\n" +
                "                  \"phone\":\"01730479817\",\n" +
                "                  \"userdonation\":{  \n" +
                "                     \"donation_times\":0,\n" +
                "                     \"donation_last\":null,\n" +
                "                     \"donation_status\":null,\n" +
                "                     \"blood_group\":\"A+\"\n" +
                "                  },\n" +
                "                  \"last_name\":\"udaya\",\n" +
                "                  \"id\":17,\n" +
                "                  \"first_name\":\"udaya\",\n" +
                "                  \"email\":\"rookchessman@gmail.com\"\n" +
                "               },\n" +
                "               \"message\":\"udaya hello\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"message\":\"random message 1\",\n" +
                "         \"id\":105\n" +
                "      },\n" +
                "      {  \n" +
                "         \"at_user\":{  \n" +
                "            \"full_name\":\"Razin Bashar Sakib\",\n" +
                "            \"avatar_url\":\"https://api.androidhive.info/json/movies/1.jpg\",\n" +
                "\t\t\t\"slug\": \"razin-bashar-sakib-1\",\n" +
                "            \"phone\":\"01724131216\",\n" +
                "            \"userdonation\":{  \n" +
                "               \"donation_times\":0,\n" +
                "               \"donation_last\":null,\n" +
                "               \"donation_status\":\"ready\",\n" +
                "               \"blood_group\":\"O+\"\n" +
                "            },\n" +
                "            \"last_name\":\"Sakib\",\n" +
                "            \"id\":24,\n" +
                "            \"first_name\":\"Razin\",\n" +
                "            \"email\":\"razinbuet334@gmail.com\"\n" +
                "         },\n" +
                "         \"created_at\":\"2018-05-18T15:20:00.590276\",\n" +
                "         \"post_comments\":[  \n" +
                "            {  \n" +
                "               \"created_at\":\"2018-05-18T15:20:00.606797\",\n" +
                "               \"writer\":{  \n" +
                "                  \"full_name\":\"Rook Chessman\",\n" +
                "                  \"avatar_url\":\"https://api.androidhive.info/json/movies/1.jpg\",\n" +
                "\t\t\t\t  \"slug\": \"rook-chessman-1\",\n" +
                "                  \"phone\":\"01730479817\",\n" +
                "                  \"userdonation\":{  \n" +
                "                     \"donation_times\":0,\n" +
                "                     \"donation_last\":null,\n" +
                "                     \"donation_status\":null,\n" +
                "                     \"blood_group\":\"A+\"\n" +
                "                  },\n" +
                "                  \"last_name\":\"Chessman\",\n" +
                "                  \"id\":17,\n" +
                "                  \"first_name\":\"Rook\",\n" +
                "                  \"email\":\"rookchessman@gmail.com\"\n" +
                "               },\n" +
                "               \"message\":\"zzz\"\n" +
                "            },\n" +
                "            {  \n" +
                "               \"created_at\":\"2018-05-18T15:20:00.606797\",\n" +
                "               \"writer\":{  \n" +
                "                  \"full_name\":\"Rooky\",\n" +
                "                  \"avatar_url\":\"https://api.androidhive.info/json/movies/1.jpg\",\n" +
                "\t\t\t\t  \"slug\": \"razin-bashar-sakib-1\",\n" +
                "                  \"phone\":\"01730479817\",\n" +
                "                  \"userdonation\":{  \n" +
                "                     \"donation_times\":0,\n" +
                "                     \"donation_last\":null,\n" +
                "                     \"donation_status\":null,\n" +
                "                     \"blood_group\":\"A+\"\n" +
                "                  },\n" +
                "                  \"last_name\":\"Chessman\",\n" +
                "                  \"id\":17,\n" +
                "                  \"first_name\":\"Rooky\",\n" +
                "                  \"email\":\"rookchessman@gmail.com\"\n" +
                "               },\n" +
                "               \"message\":\"yahoo i get it\"\n" +
                "            }\n" +
                "         ],\n" +
                "         \"message\":\"msg from razin\",\n" +
                "         \"id\":106\n" +
                "      }\n" +
                "   ],\n" +
                "    \"activeMembers\": [\n" +
                "        {\n" +
                "            \"id\": 15,\n" +
                "            \"first_name\": \"Mehran\",\n" +
                "            \"last_name\": \"Kader\",\n" +
                "            \"slug\": \"mehran-kader-1\",\n" +
                "            \"full_name\": \"Mehran Kader\",\n" +
                "            \"phone\": \"01730019842\",\n" +
                "            \"email\": \"mehran.kingfrost@gmail.com\",\n" +
                "            \"avatar_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/default_bf.png\",\n" +
                "            \"userdonation\": {\n" +
                "                \"blood_group\": \"A+\",\n" +
                "                \"donation_status\": \"ready\",\n" +
                "                \"donation_times\": 0,\n" +
                "                \"donation_last\": null\n" +
                "            },\n" +
                "            \"locations_data\": [\n" +
                "                {\n" +
                "                    \"country\": \"Bangladesh\",\n" +
                "                    \"state\": null,\n" +
                "                    \"province\": null,\n" +
                "                    \"district\": null,\n" +
                "                    \"city\": null,\n" +
                "                    \"address\": null,\n" +
                "                    \"latitude\": 23.7451259,\n" +
                "                    \"longitude\": 90.3730927\n" +
                "                }\n" +
                "            ],\n" +
                "            \"userinfo\": {\n" +
                "                \"school\": null,\n" +
                "                \"college\": null,\n" +
                "                \"university\": null,\n" +
                "                \"job\": \"sad\"\n" +
                "            },\n" +
                "            \"cover_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/default_cp.png\"\n" +
                "        },\n" +
                "        {\n" +
                "            \"id\": 16,\n" +
                "            \"first_name\": \"Udaya\",\n" +
                "            \"last_name\": \"Bir\",\n" +
                "            \"slug\": \"udaya-bir-1\",\n" +
                "            \"full_name\": \"Udaya Bir\",\n" +
                "            \"phone\": \"01671701168\",\n" +
                "            \"email\": \"udayabuet11@gmail.com\",\n" +
                "            \"avatar_url\": \"https://api.androidhive.info/json/movies/1.jpg\",\n" +
                "            \"userdonation\": {\n" +
                "                \"blood_group\": \"A+\",\n" +
                "                \"donation_status\": \"recuperating\",\n" +
                "                \"donation_times\": 0,\n" +
                "                \"donation_last\": null\n" +
                "            },\n" +
                "            \"locations_data\": [\n" +
                "                {\n" +
                "                    \"country\": \"Wakhanda\",\n" +
                "                    \"state\": null,\n" +
                "                    \"province\": null,\n" +
                "                    \"district\": null,\n" +
                "                    \"city\": null,\n" +
                "                    \"address\": null,\n" +
                "                    \"latitude\": 23.7450896,\n" +
                "                    \"longitude\": 90.3731243\n" +
                "                }\n" +
                "            ],\n" +
                "            \"userinfo\": null,\n" +
                "            \"cover_url\": null\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) jsonParser.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    @GetMapping(value = "api/circle/pending/")
    public JSONObject pending(@RequestParam String query){
        System.out.println(query);
        String s= "{\n" +
                "    \"success\": true,\n" +
                "    \"user\": 15,\n" +
                "    \"token\": \"8b6da13dc8d9e6ae7e8cd577f0a5ef02ac0ecf2f\",\n" +
                "    \"pending\": [\n" +
                "        {\n" +
                "            \"id\": 15,\n" +
                "            \"first_name\": \"Mehran\",\n" +
                "            \"last_name\": \"Kader\",\n" +
                "            \"slug\": \"mehran-kader-1\",\n" +
                "            \"full_name\": \"Mehran Kader\",\n" +
                "            \"phone\": \"01730019842\",\n" +
                "            \"email\": \"mehran.kingfrost@gmail.com\",\n" +
                "            \"avatar_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/default_bf.png\",\n" +
                "            \"userdonation\": {\n" +
                "                \"blood_group\": \"A+\",\n" +
                "                \"donation_status\": \"ready\",\n" +
                "                \"donation_times\": 0,\n" +
                "                \"donation_last\": null\n" +
                "            },\n" +
                "            \"locations_data\": [\n" +
                "                {\n" +
                "                    \"country\": \"Bangladesh\",\n" +
                "                    \"state\": null,\n" +
                "                    \"province\": null,\n" +
                "                    \"district\": null,\n" +
                "                    \"city\": null,\n" +
                "                    \"address\": null,\n" +
                "                    \"latitude\": 23.7451259,\n" +
                "                    \"longitude\": 90.3730927\n" +
                "                }\n" +
                "            ],\n" +
                "            \"userinfo\": {\n" +
                "                \"school\": null,\n" +
                "                \"college\": null,\n" +
                "                \"university\": null,\n" +
                "                \"job\": \"sad\"\n" +
                "            },\n" +
                "            \"cover_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/default_cp.png\"\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) jsonParser.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
