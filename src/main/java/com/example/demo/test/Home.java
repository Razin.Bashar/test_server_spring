package com.example.demo.test;

import org.jose4j.json.internal.json_simple.JSONObject;
import org.jose4j.json.internal.json_simple.parser.JSONParser;
import org.jose4j.json.internal.json_simple.parser.ParseException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Asus on 7/8/2018.
 */
@RestController
public class Home {
    @GetMapping(value = "api/home/")
    public JSONObject ciclelist(HttpServletRequest request){
        String s = "{\n" +
                "    \"success\": true,\n" +
                "    \"user\": 24,\n" +
                "    \"token\": \"8b6da13dc8d9e6ae7e8cd577f0a5ef02ac0ecf2c\",\n" +
                "    \"is_self\": true,\n" +
                "    \"userData\": {\n" +
                "        \"id\": 24,\n" +
                "        \"first_name\": \"Razin\",\n" +
                "        \"last_name\": \"Sakib\",\n" +
                "        \"slug\": \"razin-bashar-sakib-1\",\n" +
                "        \"full_name\": \"Razin Bashar Sakib\",\n" +
                "        \"phone\": \"01724131216\",\n" +
                "        \"email\": \"razinbuet334@gmail.com\",\n" +
                "        \"avatar_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/9b2aaad4-7b7d-11e8-8394-ea00c0982401.jpeg\",\n" +
                "        \"userdonation\": {\n" +
                "            \"blood_group\": \"A+\",\n" +
                "            \"donation_status\": \"reserved\",\n" +
                "            \"donation_times\": 0,\n" +
                "            \"donation_last\": null\n" +
                "        },\n" +
                "        \"locations_data\": [\n" +
                "            {\n" +
                "                \"country\": \"Bangladesh\",\n" +
                "                \"state\": null,\n" +
                "                \"province\": null,\n" +
                "                \"district\": null,\n" +
                "                \"city\": null,\n" +
                "                \"address\": null,\n" +
                "                \"latitude\": 23.7451099,\n" +
                "                \"longitude\": 90.3731415\n" +
                "            }\n" +
                "        ],\n" +
                "        \"userinfo\": {\n" +
                "            \"school\": \"kope\",\n" +
                "            \"college\": \"poiuy\",\n" +
                "            \"university\": \"asdf\",\n" +
                "            \"job\": \"abc\"\n" +
                "        },\n" +
                "        \"cover_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/4797f830-7b7e-11e8-aeea-ea00c0982401.jpeg\"\n" +
                "    },\n" +
                "    \"postData\": [\n" +
                "        {\n" +
                "            \"id\": 5,\n" +
                "            \"at_user\": {\n" +
                "                \"id\": 24,\n" +
                "                \"first_name\": \"Razin\",\n" +
                "                \"last_name\": \"Sakib\",\n" +
                "                \"slug\": \"razin-bashar-sakib-1\",\n" +
                "                \"full_name\": \"Razin Bashar Sakib\",\n" +
                "                \"phone\": \"01724131216\",\n" +
                "                \"email\": \"razinbuet334@gmail.com\",\n" +
                "                \"avatar_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/9b2aaad4-7b7d-11e8-8394-ea00c0982401.jpeg\",\n" +
                "                \"userdonation\": {\n" +
                "                    \"blood_group\": \"A+\",\n" +
                "                    \"donation_status\": \"reserved\",\n" +
                "                    \"donation_times\": 0,\n" +
                "                    \"donation_last\": null\n" +
                "                },\n" +
                "                \"locations_data\": [\n" +
                "                    {\n" +
                "                        \"country\": \"Bangladesh\",\n" +
                "                        \"state\": null,\n" +
                "                        \"province\": null,\n" +
                "                        \"district\": null,\n" +
                "                        \"city\": null,\n" +
                "                        \"address\": null,\n" +
                "                        \"latitude\": 23.7451099,\n" +
                "                        \"longitude\": 90.3731415\n" +
                "                    }\n" +
                "                ],\n" +
                "                \"userinfo\": {\n" +
                "                    \"school\": \"kope\",\n" +
                "                    \"college\": \"poiuy\",\n" +
                "                    \"university\": \"asdf\",\n" +
                "                    \"job\": \"abc\"\n" +
                "                },\n" +
                "                \"cover_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/4797f830-7b7e-11e8-aeea-ea00c0982401.jpeg\"\n" +
                "            },\n" +
                "            \"at_circle\": null,\n" +
                "            \"writer\": {\n" +
                "                \"id\": 24,\n" +
                "                \"first_name\": \"Razin\",\n" +
                "                \"last_name\": \"Sakib\",\n" +
                "                \"slug\": \"razin-bashar-sakib-1\",\n" +
                "                \"full_name\": \"Razin Bashar Sakib\",\n" +
                "                \"phone\": \"01724131216\",\n" +
                "                \"email\": \"razinbuet334@gmail.com\",\n" +
                "                \"avatar_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/9b2aaad4-7b7d-11e8-8394-ea00c0982401.jpeg\",\n" +
                "                \"userdonation\": {\n" +
                "                    \"blood_group\": \"A+\",\n" +
                "                    \"donation_status\": \"reserved\",\n" +
                "                    \"donation_times\": 0,\n" +
                "                    \"donation_last\": null\n" +
                "                },\n" +
                "                \"locations_data\": [\n" +
                "                    {\n" +
                "                        \"country\": \"Bangladesh\",\n" +
                "                        \"state\": null,\n" +
                "                        \"province\": null,\n" +
                "                        \"district\": null,\n" +
                "                        \"city\": null,\n" +
                "                        \"address\": null,\n" +
                "                        \"latitude\": 23.7451099,\n" +
                "                        \"longitude\": 90.3731415\n" +
                "                    }\n" +
                "                ],\n" +
                "                \"userinfo\": {\n" +
                "                    \"school\": \"kope\",\n" +
                "                    \"college\": \"poiuy\",\n" +
                "                    \"university\": \"asdf\",\n" +
                "                    \"job\": \"abc\"\n" +
                "                },\n" +
                "                \"cover_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/4797f830-7b7e-11e8-aeea-ea00c0982401.jpeg\"\n" +
                "            },\n" +
                "            \"message\": \"This is a test post!\",\n" +
                "            \"created_at\": \"2018-06-18T21:30:08.511209\",\n" +
                "            \"post_comments\": [\n" +
                "                {\n" +
                "                    \"id\": 5,\n" +
                "                    \"writer\": {\n" +
                "                        \"id\": 24,\n" +
                "                        \"first_name\": \"Razin\",\n" +
                "                        \"last_name\": \"Sakib\",\n" +
                "                        \"slug\": \"razin-bashar-sakib-1\",\n" +
                "                        \"full_name\": \"Razin Bashar Sakib\",\n" +
                "                        \"phone\": \"01724131216\",\n" +
                "                        \"email\": \"razinbuet334@gmail.com\",\n" +
                "                        \"avatar_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/9b2aaad4-7b7d-11e8-8394-ea00c0982401.jpeg\",\n" +
                "                        \"userdonation\": {\n" +
                "                            \"blood_group\": \"A+\",\n" +
                "                            \"donation_status\": \"reserved\",\n" +
                "                            \"donation_times\": 0,\n" +
                "                            \"donation_last\": null\n" +
                "                        },\n" +
                "                        \"locations_data\": [\n" +
                "                            {\n" +
                "                                \"country\": \"Bangladesh\",\n" +
                "                                \"state\": null,\n" +
                "                                \"province\": null,\n" +
                "                                \"district\": null,\n" +
                "                                \"city\": null,\n" +
                "                                \"address\": null,\n" +
                "                                \"latitude\": 23.7451099,\n" +
                "                                \"longitude\": 90.3731415\n" +
                "                            }\n" +
                "                        ],\n" +
                "                        \"userinfo\": {\n" +
                "                            \"school\": \"kope\",\n" +
                "                            \"college\": \"poiuy\",\n" +
                "                            \"university\": \"asdf\",\n" +
                "                            \"job\": \"abc\"\n" +
                "                        },\n" +
                "                        \"cover_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/4797f830-7b7e-11e8-aeea-ea00c0982401.jpeg\"\n" +
                "                    },\n" +
                "                    \"message\": \"test comment\",\n" +
                "                    \"created_at\": \"2018-01-01T00:00:00\"\n" +
                "                },\n" +
                "                {\n" +
                "                    \"id\": 6,\n" +
                "                    \"writer\": {\n" +
                "                        \"id\": 24,\n" +
                "                        \"first_name\": \"Razin\",\n" +
                "                        \"last_name\": \"Sakib\",\n" +
                "                        \"slug\": \"razin-bashar-sakib-1\",\n" +
                "                        \"full_name\": \"Razin Bashar Sakib\",\n" +
                "                        \"phone\": \"01724131216\",\n" +
                "                        \"email\": \"razinbuet334@gmail.com\",\n" +
                "                        \"avatar_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/9b2aaad4-7b7d-11e8-8394-ea00c0982401.jpeg\",\n" +
                "                        \"userdonation\": {\n" +
                "                            \"blood_group\": \"A+\",\n" +
                "                            \"donation_status\": \"reserved\",\n" +
                "                            \"donation_times\": 0,\n" +
                "                            \"donation_last\": null\n" +
                "                        },\n" +
                "                        \"locations_data\": [\n" +
                "                            {\n" +
                "                                \"country\": \"Bangladesh\",\n" +
                "                                \"state\": null,\n" +
                "                                \"province\": null,\n" +
                "                                \"district\": null,\n" +
                "                                \"city\": null,\n" +
                "                                \"address\": null,\n" +
                "                                \"latitude\": 23.7451099,\n" +
                "                                \"longitude\": 90.3731415\n" +
                "                            }\n" +
                "                        ],\n" +
                "                        \"userinfo\": {\n" +
                "                            \"school\": \"kope\",\n" +
                "                            \"college\": \"poiuy\",\n" +
                "                            \"university\": \"asdf\",\n" +
                "                            \"job\": \"abc\"\n" +
                "                        },\n" +
                "                        \"cover_url\": \"https://s3-ap-southeast-1.amazonaws.com/bloodfriend-profile-picture/4797f830-7b7e-11e8-aeea-ea00c0982401.jpeg\"\n" +
                "                    },\n" +
                "                    \"message\": \"hope\",\n" +
                "                    \"created_at\": \"2018-06-29T16:21:03.789738\"\n" +
                "                }\n" +
                "            ]\n" +
                "        }\n" +
                "    ]\n" +
                "}";

        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) jsonParser.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }
}
